<?php
declare(strict_types=1);

namespace Magebit\Faq\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magebit\Faq\Model\QuestionRepository;
use Magento\Framework\Api\SortOrderBuilder;
use Magebit\Faq\Api\Data\QuestionInterface;
/**
 * Class QuestionList
 * @package Magebit\Faq\Block
 */
class QuestionList extends Template
{
    protected $searchCriteriaBuilder;
    protected $questionRepository;
    protected $sortBuilder;

    /**
     * QuestionList constructor.
     * @param Template\Context $context
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param QuestionRepository $questionRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        QuestionRepository $questionRepository,
        SortOrderBuilder $orderBuilder,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->questionRepository = $questionRepository;
        $this->sortBuilder = $orderBuilder;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        $order = $this->sortBuilder->setField(QuestionInterface::POSITION)->setAscendingDirection()->create();
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(QuestionInterface::STATUS, QuestionInterface::ENABLED)
            ->addSortOrder($order)->create();

        return $this->questionRepository->getList($searchCriteria)->getItems();

    }
}