<?php

namespace Magebit\Faq\Api\Data;
/**
 * Interface QuestionSearchResultsInterface
 * @package Magebit\Faq\Api\Data
 */
interface QuestionSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * @return array
     */
    public function getItems(): array;

    /**
     * @param array $items
     * @return QuestionSearchResultsInterface
     */
    public function setItems(array $items);

}