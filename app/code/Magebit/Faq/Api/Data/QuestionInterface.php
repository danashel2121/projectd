<?php
namespace Magebit\Faq\Api\Data;
use Magebit\Faq\Model\QuestionRepository;

/**
 * Interface QuestionInterface
 * @package Magebit\Faq\Api\Data
 */
interface QuestionInterface
{
    public const ID = "id";
    public const QUESTION = "question";
    public const ANSWER = "answer";
    public const STATUS = "status";
    public const POSITION = "position";
    public const UPDATED_AT = "updated_at";
    public const TABLE = 'faq';
    public const ENABLED = 1;
    public const DISABLED = 0;

    /**
     * @param $question string
     * @return QuestionInterface
     */
    public function setQuestion(string $question): QuestionInterface;

    /**
     * @return string|null
     */
    public function getQuestion(): ?string;

    /**
     * @param $answer string
     * @return QuestionInterface
     */
    public function setAnswer(string $answer): QuestionInterface;

    /**
     * @return string|null
     */
    public function getAnswer(): ?string;

    /**
     * @param $status int
     * @return QuestionInterface
     */
    public function setStatus(int $status): QuestionInterface;

    /**
     * @return int|null
     */
    public function getStatus(): ?int;

    /**
     * @param $position int
     * @return QuestionInterface
     */
    public function setPosition(int $position): QuestionInterface;

    /**
     * @return int|null
     */
    public function getPosition(): ?int;

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string;
}