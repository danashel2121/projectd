<?php
declare(strict_types=1);
namespace Magebit\Faq\Api;

use Magebit\Faq\Api\Data\QuestionInterface;
use Magento\Framework\Exception\StateException;
/**
 * Interface QuestionManagementInterface
 * @package Magebit\Faq\Api
 */
interface QuestionManagementInterface
{
    /**
     * @param QuestionInterface $question
     * @return boolean
     * @throws StateException
     */
    public function enableQuestion(QuestionInterface $question): bool;

    /**
     * @param QuestionInterface $question
     * @return bool
     * @throws StateException
     */
    public function disableQuestion(QuestionInterface $question): bool;
}
