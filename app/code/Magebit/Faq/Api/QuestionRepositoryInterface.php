<?php
namespace Magebit\Faq\Api;

use Magebit\Faq\Api\Data\QuestionInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface QuestionRepositoryInterface
 * @package Magebit\Faq\Api
 */
interface QuestionRepositoryInterface
{
    /**
     * @param Data\QuestionInterface $question
     * @return Data\QuestionInterface
     */
    public function save(QuestionInterface $question): QuestionInterface;

    /**
     * @param int $id
     * @return \Magebit\Faq\Api\Data\QuestionInterface
     */
    public function getById(int $id): QuestionInterface;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * @param int $id
     * @return boolean
     */
    public function deleteById(int $id): bool;

    /**
     * @param QuestionInterface $question
     * @return boolean
     */
    public function delete(QuestionInterface $question): bool;
}

