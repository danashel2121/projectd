<?php
declare(strict_types=1);
namespace Magebit\Faq\Model;

use Magebit\Faq\Api\Data\QuestionInterface;
use Magebit\Faq\Api\QuestionManagementInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\StateException;

/**
 * Class QuestionManagement
 * @package Magebit\Faq\Model
 */
class QuestionManagement implements QuestionManagementInterface
{
    /**
     * @var QuestionRepository
     */
    protected $questionRepository;

    /**
     * QuestionManagement constructor.
     * @param QuestionRepository $questionRepository
     */
    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * @inheritdoc
     */
    public function disableQuestion(QuestionInterface $question): bool
    {
        $question->setStatus(QuestionInterface::DISABLED);
        try {
            $this->questionRepository->save($question);
        } catch (CouldNotSaveException $e) {
            throw new StateException(__('Can not disable this question'));
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function enableQuestion(QuestionInterface $question): bool
    {
        $question->setStatus(QuestionInterface::ENABLED);
        try {
            $this->questionRepository->save($question);
        } catch (CouldNotSaveException $e) {
            throw new StateException(__('Can not enable this question'));
        }

        return true;
    }
}