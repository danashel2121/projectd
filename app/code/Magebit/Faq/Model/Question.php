<?php
declare(strict_types=1);
namespace Magebit\Faq\Model;

use Magento\Framework\Model\AbstractModel;
use Magebit\Faq\Model\ResourceModel\Question as QuestionResource;
use Magebit\Faq\Api\Data\QuestionInterface;

/**
 * Class Question
 * @package Magebit\Faq\Model
 */
class Question extends AbstractModel implements QuestionInterface
{
    /**
     * Question constructor
     * @return void
     */
    protected function _construct(): void
    {
        $this->_init(QuestionResource::class);

    }

    /**
     * @return string|null
     */
    public function getQuestion(): ?string
    {
        return $this->getData(QuestionInterface::QUESTION);
    }

    /**
     * @return string|null
     */
    public function getAnswer(): ?string
    {
        return $this->getData(QuestionInterface::ANSWER);
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->getData(QuestionInterface::POSITION);
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->getData(QuestionInterface::STATUS);
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->getData(QuestionInterface::UPDATED_AT);
    }

    /**
     * @param $question string
     * @return QuestionInterface
     */
    public function setQuestion(string $question): QuestionInterface
    {
        $this->setData(QuestionInterface::QUESTION, $question);
        return $this;
    }

    /**
     * @param $answer string
     * @return QuestionInterface
     */
    public function setAnswer(string $answer):QuestionInterface
    {
        $this->setData(QuestionInterface::ANSWER, $answer);
        return $this;
    }

    /**
     * @param $position int
     * @return QuestionInterface
     */
    public function setPosition(int $position): QuestionInterface
    {
        $this->setData(QuestionInterface::POSITION, $position);
        return $this;
    }

    /**
     * @param $status int
     * @return QuestionInterface
     */
    public function setStatus(int $status): QuestionInterface
    {
        $this->setData(QuestionInterface::STATUS, $status);
        return $this;
    }

    /**
     * @param string $value
     * @return int
     */
    public function stringToInt($value): int
    {
        return (int)$value;
    }

}