<?php
declare(strict_types=1);
namespace Magebit\Faq\Model;

use Magebit\Faq\Api\QuestionRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magebit\Faq\Api\Data\QuestionSearchResultsInterfaceFactory;
use Magebit\Faq\Model\ResourceModel\Question\CollectionFactory;
use Magebit\Faq\Model\ResourceModel\Question as QuestionResource;
use Magento\Framework\Api\SearchCriteria\CollectionProcessor;
use Magebit\Faq\Api\Data\QuestionInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class QuestionRepository
 * @package Magebit\Faq\Model
 */
class QuestionRepository implements QuestionRepositoryInterface
{
    protected $collectionFactory;
    protected $questionFactory;
    protected $questionResource;
    protected $resultsInterfaceFactory;
    protected $collectionProcessor;

    /**
     * QuestionRepository constructor.
     * @param CollectionFactory $collectionFactory
     * @param QuestionFactory $questionFactory
     * @param QuestionResource $questionResource
     * @param QuestionSearchResultsInterfaceFactory $resultsInterfaceFactory
     * @param CollectionProcessor $collectionProcessor
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        QuestionFactory $questionFactory,
        QuestionResource $questionResource,
        QuestionSearchResultsInterfaceFactory $resultsInterfaceFactory,
        CollectionProcessor $collectionProcessor
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->questionFactory = $questionFactory;
        $this->questionResource = $questionResource;
        $this->resultsInterfaceFactory = $resultsInterfaceFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param SearchCriteriaInterface $criteria
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria): SearchResultsInterface
    {
        $collection = $this->questionFactory->create()->getCollection();
        $this->collectionProcessor->process($criteria, $collection);
        $searchResults = $this->resultsInterfaceFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;

    }

    /**
     * @param int $id
     * @return QuestionInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id): QuestionInterface
    {
        $question = $this->questionFactory->create();

        $this->questionResource->load($question, $id);

        if (!$question->getId()) {
            throw new NoSuchEntityException;
        }

        return $question;
    }

    /**
     * @param QuestionInterface $question
     * @return QuestionInterface
     * @throws CouldNotSaveException
     */
    public function save(QuestionInterface $question): QuestionInterface
    {
        try {
            $this->questionResource->save($question);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $question;
    }

    /**
     * @param QuestionInterface $question
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(QuestionInterface $question): bool
    {
        try {
            $this->questionResource->delete($question);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param int $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $id): bool
    {
        $question = $this->getById($id);

        try {
            $this->questionResource->delete($question);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

}