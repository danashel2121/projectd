<?php
declare(strict_types=1);
namespace Magebit\Faq\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magebit\Faq\Api\Data\QuestionInterface;

/**
 * Class Question
 * @package Magebit\Faq\Model\ResourceModel
 */
class Question extends AbstractDb
{
    protected function _construct()
    {
        $this->_init(QuestionInterface::TABLE, QuestionInterface::ID);
    }
}