<?php
declare(strict_types=1);

namespace Magebit\Faq\Controller\Adminhtml\Question;

use Magento\Backend\App\Action;
use Magebit\Faq\Model\ResourceModel\Question\CollectionFactory;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magebit\Faq\Model\QuestionManagement;
use Magento\Framework\App\Action\HttpPostActionInterface;

/**
 * Class MassEnable
 * @package Magebit\Faq\Controller\Adminhtml\Question
 */
class MassEnable extends Action implements HttpPostActionInterface
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var QuestionManagement
     */
    protected $questionManagement;

    /**
     * @var RedirectFactory
     */
    protected $redirectFactory;

    /**
     * MassEnable constructor.
     * @param Action\Context $context
     * @param CollectionFactory $collectionFactory
     * @param Filter $filter
     * @param QuestionManagement $questionManagement
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Action\Context $context,
        CollectionFactory $collectionFactory,
        Filter $filter,
        QuestionManagement $questionManagement,
        RedirectFactory $redirectFactory
    )
    {
        parent::__construct($context);
        $this->redirectFactory = $redirectFactory;
        $this->questionManagement = $questionManagement;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        foreach ($collection as $question) {
            $this->questionManagement->enableQuestion($question);
        }
        $this->messageManager->addSuccess(__('Selected questions has been enabled.'));
        return $this->redirectFactory->create()->setPath('*/*/');
    }

}