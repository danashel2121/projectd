<?php
declare(strict_types=1);
namespace Magebit\Faq\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Index
 * @package Magebit\Faq\Controller\Index
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}