<?php
declare(strict_types=1);

namespace Magebit\Faq\Ui\Component\Form\Button;

use Magebit\Faq\Ui\Component\Form;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class Delete
 * @package Magebit\Faq\Ui\Component\Form\Button
 */
class Delete extends Generic implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData(): array
    {
        $data = [];
        if ($this->getQuestionId()) {
            $data = [
                'label' => __('Delete Question'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                        'Are you sure you want to delete this question?'
                    ) . '\', \'' . $this->getUrl('*/*/delete', ['id' => $this->getQuestionId()]) . '\', {"data": {}})',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

}

