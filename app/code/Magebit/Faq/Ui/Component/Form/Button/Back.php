<?php
declare(strict_types=1);
namespace Magebit\Faq\Ui\Component\Form\Button;

use Magebit\Faq\Ui\Component\Form;

/**
 * Class Back
 * @package Magebit\Faq\Ui\Component\Form\Button
 */
class Back extends Generic{

    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s'",
                $this->getBackUrl()),
            'class' => 'back'
        ];
    }

    /**
     * @return string
     */
    public function getBackUrl(): string
    {
        return $this->getUrl('*/*');

    }
}