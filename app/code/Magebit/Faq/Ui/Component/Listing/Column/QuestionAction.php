<?php
declare(strict_types=1);
namespace Magebit\Faq\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

/**
 * Class QuestionAction
 * @package Magebit\Faq\Ui\Component\Listing\Column
 */
class QuestionAction extends Column
{
    protected $url;

    /**
     * QuestionAction constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $url
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $url,
        array $components = [],
        array $data = []
    )
    {
        $this->url = $url;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])){

            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')]["edit"] = ['href'=>
                $this->url->getUrl('faq/question/edit', ['id' => $item['id']]),
                'label' =>__('Edit'),
                'hidden' => false
                ];

                $item[$this->getData('name')]["delete"] = [
                   'href'=> $this->url->getUrl('faq/question/delete', ['id' => $item['id']]),
                    'label' =>__('Delete'),
                    'data_attribute' => [
                            'mage-init' => [
                                 'button' => ['event' => 'delete']
                 ]
            ],
                    'hidden' => false
                ];
            }
        }
        return parent::prepareDataSource($dataSource);
    }
}