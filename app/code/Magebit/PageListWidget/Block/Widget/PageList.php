<?php

namespace Magebit\PageListWidget\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class PageList extends Template implements BlockInterface
{
    const DISPLAY_MODE = 'display_mode';
    const DISPLAY_MODE_SPECIFIC = 'specific';
    const ID = 'identifier';
    const SELECTED_PAGES = 'selected_pages';

    protected $_template = "widget/page-list.phtml";
    protected $pageRepositoryInterface;
    protected $searchCriteriaBuilder;

    /**
     * PageList constructor.
     *
     * @param Template\Context $context
     * @param array $data
     * @param \Magento\Cms\Api\PageRepositoryInterface $pageRepositoryInterface
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        Template\Context $context,
        array $data = [],
        \Magento\Cms\Api\PageRepositoryInterface $pageRepositoryInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        parent::__construct($context, $data);
        $this->pageRepositoryInterface = $pageRepositoryInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * Get page list based on display mode
     *
     * @return \Magento\Cms\Api\Data\PageInterface[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPageList()
    {
        if ($this->getData(self::DISPLAY_MODE) == self::DISPLAY_MODE_SPECIFIC) {
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter(self::ID, $this->getData(self::SELECTED_PAGES), 'in')
                ->create();
        } else {
            $searchCriteria = $this->searchCriteriaBuilder->create();
        }
        return $this->pageRepositoryInterface->getList($searchCriteria)->getItems();
    }

}