<?php

namespace Magebit\PageListWidget\Model;


class Pages implements \Magento\Framework\Option\ArrayInterface
{
    protected $collectionFactory;

    public function __construct(
        \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     *Creates array with pages
     *
     * @return array
     */
    public function toOptionArray()
    {
        $pages = [];
        $collection = $this->collectionFactory->create();

        foreach ($collection as $page) {
            $pages[] = [
                'value' => $page->getIdentifier(),
                'label' => $page->getTitle(),
            ];
        }

        return $pages;
    }

}

